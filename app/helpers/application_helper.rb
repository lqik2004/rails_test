module ApplicationHelper
  #Return the full title helper
  def full_title(page_title)
    base_title = "Ruby Test"
    if page_title.empty?
      base_title
    else
      "#{base_title} | #{page_title}"
    end
  end
end
